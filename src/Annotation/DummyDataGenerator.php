<?php

namespace Drupal\km_dummy_data\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Generator backend annotation object.
 *
 * @see \Drupal\km_dummy_data\Generator\GeneratorPluginManager
 * @see \Drupal\km_dummy_data\Generator\GeneratorInterface
 * @see \Drupal\km_dummy_data\Generator\GeneratorPluginBase
 * @see plugin_api
 *
 * @Annotation
 */
class DummyDataGenerator extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

}
