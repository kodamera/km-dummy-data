<?php

namespace Drupal\km_dummy_data\Generator;

use Drupal\Component\Plugin\DerivativeInspectionInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Component\Plugin\DependentPluginInterface;

/**
 * Defines an interface for Generator plugins.
 *
 * @see \Drupal\km_dummy_data\Annotation\DummyDataGenerator
 * @see \Drupal\km_dummy_data\Generator\GeneratorPluginManager
 * @see \Drupal\km_dummy_data\Generator\GeneratorPluginBase
 * @see plugin_api
 */
interface GeneratorInterface extends PluginInspectionInterface, DerivativeInspectionInterface, ContainerFactoryPluginInterface, DependentPluginInterface {

  /**
   * Generate dummy data.
   *
   * @param array
   *   An array of options.
   */
  public function generate(array $options = []);

}
