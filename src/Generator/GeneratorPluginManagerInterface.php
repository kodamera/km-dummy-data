<?php

namespace Drupal\km_dummy_data\Generator;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Provides an interface for the generator plugin manager service.
 */
interface GeneratorPluginManagerInterface extends PluginManagerInterface {

  /**
   * Returns all known generators.
   *
   * @return \Drupal\km_dummy_data\Generator\GeneratorInterface[]
   *   An array of generator plugins, keyed by type identifier.
   */
  public function getInstances();

}
