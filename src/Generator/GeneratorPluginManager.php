<?php

namespace Drupal\km_dummy_data\Generator;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Manages generator plugins.
 *
 * @see \Drupal\km_dummy_data\Annotation\DummyDataGenerator
 * @see \Drupal\km_dummy_data\Generator\GeneratorPluginInterface
 * @see \Drupal\km_dummy_data\Generator\GeneratorPluginBase
 * @see plugin_api
 */
class GeneratorPluginManager extends DefaultPluginManager implements GeneratorPluginManagerInterface {

  /**
   * Static cache for the generator plugins.
   *
   * @var \Drupal\km_dummy_data\Generator\GeneratorInterface[]|null
   *
   * @see \Drupal\km_dummy_data\Generator\GeneratorPluginManager::getInstances()
   */
  protected $generators = NULL;

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {

    parent::__construct('Plugin/km_dummy_data/generator', $namespaces, $module_handler, 'Drupal\km_dummy_data\Generator\GeneratorInterface', 'Drupal\km_dummy_data\Annotation\DummyDataGenerator');

    $this->setCacheBackend($cache_backend, 'km_dummy_data_generators');
    $this->alterInfo('km_dummy_data_generators');
  }

  /**
   * {@inheritdoc}
   */
  public function getInstances() {
    if ($this->generators === NULL) {
      $this->generators = [];

      foreach ($this->getDefinitions() as $name => $generator_definition) {
        if (class_exists($generator_definition['class']) && empty($this->generators[$name])) {
          $generator = $this->createInstance($name);
          $this->generators[$name] = $generator;
        }
      }
    }

    return $this->generators;
  }

  /**
   * {@inheritdoc}
   */
  public function clearCachedDefinitions() {
    parent::clearCachedDefinitions();

    $this->discovery = NULL;
  }

}
