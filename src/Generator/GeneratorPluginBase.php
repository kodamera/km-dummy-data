<?php

namespace Drupal\km_dummy_data\Generator;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\PluginBase;
use Faker\Factory as FakerFactory;
use Faker\Generator as FakerGenerator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a base class from which other generator classes may extend.
 *
 * Plugins extending this class need to define a plugin definition array through
 * annotation. The definition includes the following keys:
 * - id: The unique, system-wide identifier of the display class.
 *
 * A complete plugin definition should be written as in this example:
 *
 * @code
 * @DummyDataGenerator(
 *   id = "my_generator"
 * )
 * @endcode
 *
 * @see \Drupal\km_dummy_data\Annotation\DummyDataGenerator
 * @see \Drupal\km_dummy_data\Generator\GeneratorPluginManager
 * @see \Drupal\km_dummy_data\Generator\GeneratorInterface
 * @see plugin_api
 */
abstract class GeneratorPluginBase extends PluginBase implements GeneratorInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|null
   */
  protected $entityTypeManager;

  /**
   * A faker generator.
   *
   * @var \Faker\Generator|null
   */
  protected $faker;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $generator = new static($configuration, $plugin_id, $plugin_definition);

    $generator->setEntityTypeManager($container->get('entity_type.manager'));
    $generator->setFaker(FakerFactory::create());

    return $generator;
  }

  /**
   * Retrieves the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  public function getEntityTypeManager() {
    return $this->entityTypeManager ?: \Drupal::service('entity_type.manager');
  }

  /**
   * Sets the entity type manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The new entity type manager.
   *
   * @return $this
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    return $this;
  }

  /**
   * Retrieves the faker generator.
   *
   * @return \Faker\Generator
   *   The faker generator.
   */
  public function getFaker() {
    return $this->faker ?: FakerFactory::create();
  }

  /**
   * Sets the faker generator.
   *
   * @param \Faker\Generator
   *   The faker generator.
   *
   * @return $this
   */
  public function setFaker(FakerGenerator $faker) {
    $this->faker = $faker;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = [];

    // By default, add dependencies to the module providing this display and to
    // the index it is based on.
    $definition = $this->getPluginDefinition();
    $dependencies['module'][] = $definition['provider'];

    $index = $this->getIndex();
    $dependencies[$index->getConfigDependencyKey()][] = $index->getConfigDependencyName();

    return $dependencies;
  }

}
