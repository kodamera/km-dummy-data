<?php

namespace Drupal\km_dummy_data\Commands;

use Drupal\km_dummy_data\Generator\GeneratorPluginManagerInterface;
use Drush\Commands\DrushCommands;

/**
 * Defines Drush commands for KM Dummy data.
 */
class DummyDataCommands extends DrushCommands {

  /**
   * The generator plugin manager.
   *
   * @var \Drupal\km_dummy_data\Generator\GeneratorPluginManagerInterface
   */
  protected $generatorPluginManager;

  /**
   * Creates a new DummyDataCommands instance.
   *
   * @param \Drupal\km_dummy_data\Generator\GeneratorPluginManagerInterface $generator_plugin_manager
   *   The generator plugin manager.
   */
  public function __construct(GeneratorPluginManagerInterface $generator_plugin_manager) {
    $this->generatorPluginManager = $generator_plugin_manager;
  }

  /**
   * Generates dummy data.
   *
   * @param string $generator_id
   *   The id of the generator to use.
   * @param string[] $options
   *   Options from CLI.
   *
   * @option seed
   *   Optional seed value to faker.
   *
   * @usage km_dummy_data:generate my_generator --seed=100
   *   Generate dummy data with seed 100.
   *
   * @command km_dummy_data:generate
   * @aliases kmdd,kmddg
   */
  public function generate($generator_id, array $options = [
    'seed' => 666,
  ]): void {
    $plugins = $this->generatorPluginManager->getInstances();
    if (!isset($plugins[$generator_id])) {
      echo 'No plugin matching the name: ' . $generator_id;
    }
    
    $plugins[$generator_id]->generate($options);
  }

}
